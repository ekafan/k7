import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class Puzzle {

   private ArrayList<String> first;
   private ArrayList<String> second;
   private ArrayList<String> third;

   private final ArrayList<String> rightPath = new ArrayList<>();

   private final ArrayList<String> allRightPaths = new ArrayList<>();

   private static final ArrayList<String> alphabet = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F",
           "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"));

   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */
   public static void main (String[] args) {
      Puzzle a = new Puzzle();
      a.run(args);
   }

   public void run(String[] args) {

      LinkedList<String> small = convertToSmallList(args);

      first = convertToBiglList(args[0]);
      second = convertToBiglList(args[1]);
      third = convertToBiglList(args[2]);

      for (int num = 0; num < 10; num++) {
         LinkedList<String> clo = new LinkedList<>();
         clo.addAll(small);
         ArrayList<String> way = new ArrayList<>();
         putToPath(clo, 0, num, way);
      }

      int rightPathCount = allRightPaths.size();

      if (rightPathCount == 0) {
         System.out.println("This task has no answer");
      } else {
         if (rightPathCount == 1) {
            System.out.println("This task has 1 answer");
            System.out.println("Here is it");
         } else {
            System.out.println("This task has " + rightPathCount + " answers");
            System.out.println("Here is one of them");
         }

         for (String letter : rightPath) {
            if (!(letter.equals("addition"))) {
               StringBuffer answer = new StringBuffer();
               answer.append(letter);
               answer.append(" -> ");
               answer.append(rightPath.indexOf(letter));
               System.out.println(answer);
            }

         }
      }
      System.out.println("----------------");
   }


   private LinkedList<String> convertToSmallList(String[] before) {
      LinkedList<String> small = new LinkedList<>();
      for (String word : before) {
         String[] newOne = word.split("");
         for (String letter : newOne) {
            if (!(alphabet.contains(letter))) {
               throw new RuntimeException("letter " + letter + " is not in english alphabet");
            }
            if (!(small.contains(letter))) {
               small.add(letter);
            }
         }
      }
      while (small.size() < 10) {
               small.add("addition");
      }
      return small;
   }

   private ArrayList<String> convertToBiglList(String word) {
      ArrayList<String> big = new ArrayList<>();
         String[] newOne = word.split("");
         for (String letter : newOne) {
            if (!(alphabet.contains(letter))) {
               throw new RuntimeException("letter " + letter + " is not in english alphabet");
            }
            big.add(letter);
         }

      return big;
   }


   private String putToPath(LinkedList<String> base, int step, int path, ArrayList<String> way) {

      String name;
      if (step == 0) {

         name = base.get(path);
         base.remove(name);
      } else {
         name = base.pop();
      }

      way.add(name);

      if (base.size() == 0) {
         if (pathCheck(way)) {
            boolean contains = false;

            for (String variants : allRightPaths) {
               String wayStr = String.join("", way);
               String varStr = String.join("", variants);

               if (wayStr.equals(varStr)) {
                  contains = true;
               }
            }
            if (!(contains)) {
               String toAdd = String.join("", way);
               allRightPaths.add(toAdd);
            }
            if (rightPath.isEmpty()) {
               rightPath.addAll(way);
            }
         }
         way.remove(way.size()-1);
         return name;
      }


      for (String character : base) {
         LinkedList<String> clone = new LinkedList<>();
         clone.addAll(base);

         clone.remove(character);
         clone.push(character);

         putToPath(clone, step + 1, path, way);
      }
      way.remove(way.size()-1);
      return name;
   }


   private boolean pathCheck(ArrayList<String> way) {
      if (way.indexOf(first.get(0)) == 0 || way.indexOf(second.get(0)) == 0 || way.indexOf(third.get(0)) == 0) {
         return false;
      }

      StringBuffer firstStr = new StringBuffer();
      StringBuffer secondStr = new StringBuffer();
      StringBuffer thirdStr = new StringBuffer();

      for (String letter1 : first) {
         firstStr.append(way.indexOf(letter1));
      }

      for (String letter2 : second) {
         secondStr.append(way.indexOf(letter2));
      }

      for (String letter3 : third) {
         thirdStr.append(way.indexOf(letter3));
      }

      long addend1;
      long addend2;
      long addend3;

      try {
         addend1 = Long.parseLong(String.valueOf(firstStr));
         addend2 = Long.parseLong(String.valueOf(secondStr));
         addend3 = Long.parseLong(String.valueOf(thirdStr));
      }
      catch (NumberFormatException e){
         System.out.println("caught an error");
         return false;
      }

      return addend1 + addend2 == addend3;
   }
}

